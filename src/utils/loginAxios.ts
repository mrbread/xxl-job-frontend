import Axios from 'axios'
import { store } from '@/store'

interface AxiosConfig {
  timeout: number
  headers: {
    'Content-Type': string
  }
}

const config: AxiosConfig = {
  timeout: 600000,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
}

const axios = Axios.create(config)

// 请求前拦截
axios.interceptors.request.use(
  (req) => {
    const { token = '' } = store.getState().user.UserInfo || {}
    req.headers.token = token
    return req
  },
  (err) => {
    return Promise.reject(err)
  }
)

// post请求
// @ts-ignore
axios.post = (url: string, params?: object): Promise<any> =>
  axios({
    method: 'post',
    url,
    data: params
  })

export default axios
