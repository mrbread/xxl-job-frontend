import { HomeOutlined, BarsOutlined, BarChartOutlined } from '@ant-design/icons'
import Home from '@/pages/home'
import { MenuRoute } from '@/route/types'
// import React from 'react'
// import { Icon } from '@iconify/react'
import TaskList from '@/pages/task'
import JobLogList from '@/pages/jobLog'

/**
 * path 跳转的路径
 * component 对应路径显示的组件
 * exact 匹配规则，true的时候则精确匹配。
 */
const preDefinedRoutes: MenuRoute[] = [
  {
    path: '/',
    name: '首页',
    exact: true,
    key: 'home',
    icon: HomeOutlined,
    component: Home
  },
  {
    path: '/taskList',
    name: '任务列表',
    exact: true,
    key: '/taskList',
    icon: BarsOutlined,
    // hideInMenu: true,
    component: TaskList
  },
  {
    path: '/jobLogList',
    name: '调度日志',
    exact: true,
    key: '/jobLogList',
    icon: BarChartOutlined,
    // hideInMenu: true,
    component: JobLogList
  }
]

export default preDefinedRoutes
