import React, { FC } from 'react'
import { Card, Row, Col } from 'antd'

const Home: FC = () => {
  return (
    <div className="home" style={{ height: '100vh', padding: 20 }}>
      <Row gutter={16}>
        <Col span={24} style={{ marginTop: 20 }}>
          <Card>欢迎使用xxl-job!</Card>
        </Col>
      </Row>
    </div>
  )
}

export default Home
