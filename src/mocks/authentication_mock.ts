import type { UserInfo } from '@/app_models/user'

export const userRes: UserInfo[] = [
  {
    username: 'admin',
    password: '123456',
    token: 'asdfghjkl',
    permission: [
      {
        code: '/taskList',
        name: '任务列表'
      },
      {
        code: '/jobLogList',
        name: '调度日志'
      }
    ]
  }
]
