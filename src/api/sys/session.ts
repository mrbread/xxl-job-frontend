import $axios from '@/utils/loginAxios'
import { UserInfo } from '@/app_models/user'
import { userRes } from '@/mocks/authentication_mock'

export default {
  // 登陆
  async login(userName: string, password: string): Promise<UserInfo> {
    const res = await $axios.post(
      `/api/login?userName=${userName}&password=${password}`
    )
    if (res?.data?.code === 200) {
      return userRes[0]
    }
    throw new Error(res?.data?.msg)
  },
  async logout(): Promise<boolean> {
    const res = await $axios.post('/api/logout')
    return res?.data?.code === 200
  }
}
