import $axios from '@/utils/axios'

export default {
  // 获取任务列表数据
  getTaskList(params?: object): Promise<CommonObjectType<string>> {
    return $axios.get('/api/jobinfo/pageList', params)
  },
  // 获取调度日志数据
  getJobLogList(params?: object): Promise<CommonObjectType<string>> {
    return $axios.get('/api/joblog/pageList', params)
  }
}
