const webpackConfig = require('./config/webpack.config')

const { overrideDevServer } = require('customize-cra')
module.exports = {
  webpack: webpackConfig,
  //处理跨域
  devServer: overrideDevServer((config) => {
    config.proxy = {
      '/api/': {
        target: 'http://localhost:8080/xxl-job-admin',
        changeOrigin: true,
        pathRewrite: { '^/api/': '/' }
      }
    }
    return config
  })
}
